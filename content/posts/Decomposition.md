---
title: "Decomposition of unitary matrix"
date: 2022-06-12T19:51:51+08:00
katex: true
draft: true
markup: 'mmark'
---
### Unitary Operator 
A quantum gate can be represented as an unitary operator. When the basis for computation has been chosen, the unitary operator can be represented using unitary matrix. A matrix A is unitary if 

$$AA^{*}=I$$ 

where \\(A^*\\) is the hermitian conjugate of the matrix A. 

In many cases, the unitary matrix is readily available (Or steal them like you steal hamiltonians). To convert this unitary into a form of feasible computation, one needs to convert the unitary into a set of known gates. Ever since the universailty of many quantum gates has been proven, many quantum computers have been built with an array of universal gates and the programmer has to convert his unitary matrix into a quantum circuit containing only the set of gates available in the hardware. We would like to map our unitary into these fixed set of unitary. The process of converting an unitary matrix into a set of unitary matrices is called decomposition. This set of unitary matrices belong to a library of unitaries and applying them in a sequence has the same effect as the original unitary which was decomposed. 

Decomposition of unitary matrix is an essential part to quantum computing. Efficient decompistion is required so that the complexity of the original problem is not augmented after the process of decomposition. This can depend on the availble set of quantum gates, the process used for decomposition etc. The number of gates used in decomposition is also another paramter that you want to optimise for. A decomposition that uses lesser number of gates are considered better than a one that uses more. 

Let us consider a 4 x 4 unitary matrix. This represents a 2 qubit quantum gate. We would like to decompose this arbitrary 2 qubit gate usinga set of known one/two qubit gates. 


<!-- The first step towards achieving this is converting the existing N dimensional unitary into a series of 2 or 1 qubit operation. The unitary in hand might be a n-qubit unitary, i.e it operates on aset of n qubits. But most popular quantum hardware only provides 1 or 2 qubit gates and we need to convert then-qubit unitary into a series of one/two qubit operations.  -->













[^1]:https://arxiv.org/pdf/1210.7366.pdf
[^2]:https://jxshix.people.wm.edu/Math410-2014/Li-14W&M.pdf
[^3]:
