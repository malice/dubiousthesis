---
title: "Mutliplexing Channels in golang"
date: 2022-07-31T06:06:34+08:00
draft: false
tags: [golang, concurrency]
---

Multiplexing channels in golang is an interesting problem. When a channel is created in go, it can be used to send messages by anyone who has the (object) channel. So a channel can be passed around to multiple threads and they can write to the channel as they require. The below example spawns two threads to write concurrently to a channel and the main thread prints out any messages received in the channel.

```go 

package main

import (
	"fmt"
	"sync"
)

func main() {
	a := make(chan int)
	var wg sync.WaitGroup
	wg.Add(1)

	go func(_a chan int) {
		for i := 0; i < 1000; i++ {
			_a <- i
		}
		wg.Done()

	}(a)
	wg.Add(1)
	go func(_a chan int) {
		for i := 1000; i < 2000; i++ {
			_a <- i
		}
		wg.Done()

	}(a)
	go func() {
		for v := range a {
			fmt.Printf("Received message: %d\n", v)
		}
	}()

	wg.Wait()
	close(a)

}
```

But reading from a channel is interesting. What happens if there are two readers for a channel ?

```go
package main

import (
	"fmt"
	"sync"
)

func main() {
	a := make(chan int)
	var wg sync.WaitGroup
	wg.Add(1)

	go func(_a chan int) {
		for i := 0; i < 1000; i++ {
			_a <- i
		}
		wg.Done()

	}(a)
	wg.Add(1)
	go func(_a chan int) {
		for i := 1000; i < 2000; i++ {
			_a <- i
		}
		wg.Done()

	}(a)
	var wgr sync.WaitGroup
	wgr.Add(1)
	go func(_a chan int) {
		for v := range _a {
			fmt.Printf("From 1 listener: %d\n", v)
		}
		wgr.Done()
	}(a)
	go func(_a chan int) {
		for v := range _a {
			fmt.Printf("From 2nd listener: %d\n", v)
		}
		wgr.Done()
	}(a)
	wg.Wait()
	close(a)
	wgr.Wait()
}


```

Both the threads receive data from the channel, but the never receive the same item.  
If the channel has to be used to send the data to all threads that is listening to the channel, this is not a good setup. If it was possible, it might have been a good way to carry out concurrent processing on the same data. It will also be non-trivial to setup a pubsub model using channels. The channels cannot be used to send data to multiple parties. To circumvent this, multiplexing channels manually is a plausible idea.

```go

package main

import (
	"fmt"
	"sync"
)

func main() {
	a := make(chan int)
	sinks := []chan int{make(chan int), make(chan int)}
	var wg sync.WaitGroup
	wg.Add(1)

	go func(_a chan int) {
		for i := 0; i < 1000; i++ {
			_a <- i
		}
		wg.Done()

	}(a)
	wg.Add(1)

	go func(_a chan int) {
		for i := 1000; i < 2000; i++ {
			_a <- i
		}
		wg.Done()

	}(a)

	multiplexChannels(a, sinks)

	var wgr sync.WaitGroup
	wgr.Add(1)
	go func(_a chan int) {
		for v := range _a {
			fmt.Printf("From 1 listener: %d\n", v)
		}
		wgr.Done()
	}(sinks[0])
	go func(_a chan int) {
		for v := range _a {
			fmt.Printf("From 2nd listener: %d\n", v)
		}
		wgr.Done()
	}(sinks[1])
	wg.Wait()
	close(a)
	wgr.Wait()
}

func multiplexChannels(source chan int, sinks []chan int) {
	go func() {
		for v := range source {
			for _, s := range sinks {
				s <- v
			}
		}
		for _, s := range sinks {
			close(s)
		}
	}()
}

```

There is a major drawback in the above code.

1. Multiplex channel expects that there are active receivers listening to the sinks. If one of the sink is not used or used by a _slow_ processor, it will slow down the whole multiplexed queue.

A quick trick to multiplex one message to all listeners is to use the close channel event. When a channel is closed all the receivers receive the `closed event`. Since a channel can only be closed once, this method can only be used once. This a popular use case, especially in creating `done` channels in golang, to notifier receivers that a task has been done.

```go
package main

import (
	"fmt"
	"sync"
)

func main() {
	a := make(chan int)

	var wg sync.WaitGroup
	wg.Add(1)

	go func(_a chan int) {
		for i := 0; i < 1000; i++ {
			_a <- i
		}
		wg.Done()

	}(a)
	wg.Add(1)

	go func(_a chan int) {
		for i := 1000; i < 2000; i++ {
			_a <- i
		}
		wg.Done()

	}(a)

	var wgr sync.WaitGroup
	wgr.Add(1)
	go func(_a chan int) {
		for _ = range _a {
		}
		fmt.Println("Received close Event1")
		wgr.Done()
	}(a)
	wgr.Add(1)
	go func(_a chan int) {
		for _ = range _a {
		}
		fmt.Println("Received close Event2")
		wgr.Done()
	}(a)
	wg.Wait()
	close(a)
	wgr.Wait()
}

```

Another popular alternative to golang concurrent processing is pipeline architecture. Read about it [ here ](https://go.dev/blog/pipelines)
