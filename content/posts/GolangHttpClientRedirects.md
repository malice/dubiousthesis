---
title: "Golang, Client and Redirect"
date: 2022-06-04T23:51:50+08:00
tags: [golang, http]
draft: false
---

It is counter inuitive how `golang http client `behaves when it receives a redirect response. It follows redirects silently by default and do not notify the caller. This can become quite confusing[^1][^2] when there are errors on the redirected URL. For example, in the case when you do an http to https redirect, all the headers set for the http url are not passed along for the https call after redirect. If there is a bearer token in the request,  now  developer is gaping at a 401 unauthorised reply from server because the token was not passed along in the second(redirected) request. All this happens behind the scenes and the developer only sees the request being sent to the server and the server responding with a `Unauthorised 401` response. Since the redirect `3xx` response is lost for ever, it causes quite a lot of inconvenience to the developer too[^3]. 

The below code redirects you to google without even telling you. This is probably the right behaviour, but considering that the request changes its structure while following the redirect,  the redirect should have been notified back to the caller.

```go
package main
import (
  "fmt"
  "io/ioutil"
  "net/http"
  "time"
)
func main() {
    client := http.Client{Timeout: time.Duration(1) * time.Second}
    resp, err := client.Get("http://shorturl.at/lzCYZ")
    if err != nil {
        fmt.Printf("Error %s", err)
        return
    }
    defer resp.Body.Close()
    body, err := ioutil.ReadAll(resp.Body)

    if err != nil {
      fmt.Printf("Error %s", err)
      return
    }

    fmt.Printf("Body : %s", body)
}

```

Inorder to ask the `golang http client `not to follow the redirects, one has to explicitly ask the client to do so:

```go
client := &http.Client{
    CheckRedirect: func(req *http.Request, via []*http.Request) error {
        return http.ErrUseLastResponse
    },
}
```
In comparison, using python `requests` library, one has to explicitly ask the client to follow redirects.
```python
import requests

ans = requests.post( "http://shorturl.at/lzCYZ",  allow_redirects=True)

```

[^1]: https://github.com/golang/go/issues/42832
[^2]: https://github.com/golang/go/issues/20042 
[^3]: https://github.com/gocolly/colly/issues/298
