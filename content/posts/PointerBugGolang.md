---
title: "Pointers and for loop variables in golang"
date: 2022-06-20T21:36:13+08:00
tags: [golang ]
draft: false
---

The structure of `for` loops in new programming languages are very readable and easier to comprehend. For example, in golang, a `for` loop can be written as 

```go
for i, v := range array {

....

}

```
And this is much more readable than 

```go

for (i:=0; i < len(array); i++){
v := a[i];

....

}
```
All is good, until you involve the pointers.


Consider the following program: 

```go
package main

import "fmt"

type Container struct {
	Field *string
}

func main() {
	containers := []Container{}
	for _, val := range []string{"one", "two", "three"} {
		c := Container{&val}
		containers = append(containers, c)

	}
	for _, val := range containers {
		fmt.Println(*val.Field)
	}

}
```
On running the above code , the result is surprising. The container list has the same value in all 3 containers.  


To understand this, the initialisation in  `for` loops should be rewritten as 

```go
var v string
for _, val = range []{"one","two", "three"} {

...
}

```

Now what happens is bit more clearer. The pointer to `v` is same in all 3 iterations of the loop. So you are effectively putting the same pointer in  all the containers. If we had used the value instead of v, the values are coopied across to the Container rather than storing the pointer to v in the container. 

```go

package main

import "fmt"

type Container struct {
	Field string
}

func main() {
	containers := []Container{}
	for _, val := range []string{"one", "two", "three"} {
		c := Container{val}
		containers = append(containers, c)
	}
	for _, val := range containers {
		fmt.Println(val.Ref.Field)
	}

}

```
