---
title: "Hamiltonian"
date: 2022-06-06T19:29:09+08:00
katex: true
draft: false
markup: 'mmark'
tags: ['QM']
---

# What is so special about Hamiltonian ?

Hamiltonians are omnipresent in any discussion that relates to quantum mechanics. Even with such importance, the meaning of **hamiltonian** remains in the shades, especially for someone new to quantum mechanics. The most common explanation given for a hamiltonian is that it is the energy operator for the system. It's eigen values **defines** the possible energy values that the system can take. Hopefully by the end of this blog, you get another perspective about the hamiltonian. 

# States of the system.


In classical physics, state of the system is represented by position and momentum of particle. This perfectly matches our everyday experience - when one says, the system(particle) is at position x and momentum p, these can be confirmed by measuring the position and velocity of the particle. Knowing this state is all we require to predict the future state of the particle. This conforms to our every-day logic. 

In quantum mechanics, the state of the system is represented by a vector in state space 

$$\ket\psi$$ 

This vector is all that we need to know about the system to predict the future state (the vector!)  of the particle. A typical  state that changes with time is represented by defining the state as a function of time. 

$$\ket{\psi(t)}$$


The change in state can be represented using a unity operator, U, where

$$\ket{\psi(t)}= U\ket{\psi(0)}$$

A boring unitary operator is \\(I\\) - the identity operator (or matrix)

$$\ket{\psi(t)} = I \ket{\psi(0)}$$

The above equation means that the state of the system does not change with time. But this is what happens across a small period of time state remains almost the same, barring a slight deviation which can be written as :


$$\ket{\psi(\epsilon)}=(I - \epsilon H) \ket{\psi(0)} $$


Now expanding and rearranging the terms, you can get \\( \frac {\partial\psi}{\partial t}\\) on the LHS. Sounds familiar ?
$$\frac {\partial\psi}{\partial t} = H \psi$$
YES!! It is the schrodinger equation (barring some missing constants). So Hamiltonian is something that defines how the state of the system changes over time!!!  
